//
//  AlbumTableViewCell.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit
import AlamofireImage

class AlbumTableViewCell: UITableViewCell {

    
    @IBOutlet weak var albumNameLabelLeading: NSLayoutConstraint!
    @IBOutlet weak var albumCoverImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var albumCoverImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var albumCoverImageView: UIImageView!
    @IBOutlet weak var downloadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var albumNameLabel: UILabel!
    
    fileprivate let imageTransitionSpeed = 0.5
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureBy(album: Album, height: CGFloat) {
        albumNameLabel.text = album.name
        albumCoverImageViewWidth.constant = height
        albumCoverImageViewHeight.constant = height
        albumCoverImageView.frame.size = CGSize(width: height, height: height)
        albumCoverImageView.layer.cornerRadius = albumCoverImageView.frame.width / 2
        albumCoverImageView.layer.masksToBounds = true
        self.separatorInset.left = albumCoverImageView.frame.origin.x + height + albumNameLabelLeading.constant
        
        let url = URL(string: album.coverUrl)
        self.albumCoverImageView.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.global(), imageTransition: UIImageView.ImageTransition.crossDissolve(1), runImageTransitionIfCached: false) { (comp) in
            if comp.result.isSuccess {
                album.coverData = UIImagePNGRepresentation(comp.result.value!)
            } else {
                if let savedData = album.coverData {
                    self.albumCoverImageView.image = UIImage(data: savedData)
                }
                else {
                    self.albumCoverImageView.image = UIImage(named: "photoNotSaved")
                }
            }
            self.downloadIndicator.stopAnimating()
        }
        
        

    }

}
