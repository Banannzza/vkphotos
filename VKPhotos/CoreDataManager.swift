//
//  CoreDataManager.swift
//  KiT
//
//  Created by Maxim Aliev on 10/05/16.
//  Copyright © 2016 maxial. All rights reserved.
//

import UIKit
import CoreData


class CoreDataManager {
    
    static let managedContext =
        (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    
    class func createObjectForEntity(entityName name: String) -> NSManagedObject? {
        return NSEntityDescription.insertNewObject(forEntityName: name, into: managedContext)
    }
    
    class func loadObjectsForEntity(entityName name: String) -> [NSManagedObject]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: name)
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            if results.count == 0 {
                return nil
            }
            return results as? [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    class func save() {
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    class func deleteObject(_ object: NSManagedObject) {
        managedContext.delete(object)
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
}










