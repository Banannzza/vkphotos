//
//  AlbumPhotoTableViewCell.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit

class AlbumPhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var photoNameTextViewLeading: NSLayoutConstraint!
    @IBOutlet weak var photoImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var photoImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var downloadIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var photoDateLabel: UILabel!
    @IBOutlet weak var photoNameTextView: UITextView!
    
    fileprivate let imageTransitionSpeed = 0.5
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configureBy(photo: Photo, height: CGFloat) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "RU")
        dateFormatter.dateStyle = DateFormatter.Style.medium
        photoDateLabel.text = dateFormatter.string(from: photo.date as Date)
        photoNameTextView.text = photo.name
        photoNameTextView.textColor = UIColor.white
        
        photoImageViewWidth.constant = height
        photoImageViewHeight.constant = height
        photoImageView.frame.size = CGSize(width: height, height: height)
        photoImageView.layer.cornerRadius = photoImageView.frame.width / 2
        photoImageView.layer.masksToBounds = true
        self.separatorInset.left = photoImageView.frame.origin.x + height + photoNameTextViewLeading.constant
        let url = URL(string: photo.previewImageUrl)
        self.photoImageView.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.global(), imageTransition: UIImageView.ImageTransition.crossDissolve(imageTransitionSpeed), runImageTransitionIfCached: false, completion: { (comp) in
            if comp.result.isSuccess {
                photo.previewImageData = UIImagePNGRepresentation(comp.result.value!)
            } else {
                if let savedData = photo.previewImageData {
                    self.photoImageView.image = UIImage(data: savedData)
                }
                else {
                    self.photoImageView.image = UIImage(named: "photoNotSaved")
                }
            }

            self.downloadIndicatorView.stopAnimating()
        })
    }
    
}
