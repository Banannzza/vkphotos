//
//  Album.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import Foundation
import CoreData

class Album: NSManagedObject {
    @NSManaged var id: NSNumber
    @NSManaged var name: String
    @NSManaged var coverUrl: String
    @NSManaged var coverData: Data?
    @NSManaged var photos: NSMutableOrderedSet
    
    func configureBy(dictionary: NSDictionary) {
        self.id = dictionary["id"] as! NSNumber
        self.name = dictionary["title"] as! String
        for image in dictionary["sizes"] as! [NSDictionary] {
            let imageType = image["type"] as! String
            if imageType == "x" {
                self.coverUrl = image["src"] as! String
            }

        }

    }
}
