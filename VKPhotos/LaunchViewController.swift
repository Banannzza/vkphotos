//
//  LaunchViewController.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit
import VK_ios_sdk
import Reachability
import GoogleMaps

class LaunchViewController: UIViewController {
    
    
 
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var quitButton: UIButton!
    @IBOutlet weak var actionButtonWidth: NSLayoutConstraint!
    
    fileprivate var originButtonWidth: CGFloat!
    fileprivate var reach: Reachability?
    fileprivate let permission = ["photos", "wall"]
    fileprivate let animationTime = 0.5

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.originButtonWidth = self.actionButtonWidth.constant
        self.actionButton.layer.cornerRadius = 10
        self.quitButton.layer.cornerRadius = 10
        self.originButtonWidth = self.view.frame.width / 2
        self.informationLabel.alpha = 0
        
        if !VKSdk.initialized() {
            let vksInstance = VKSdk.initialize(withAppId: "5679917")
            vksInstance?.uiDelegate = self
            vksInstance?.register(self)
        }

        do {
            self.reach = try Reachability()
            if !reach!.isReachable {
                internetConnectionDisabled()
            } else {
                VKSdk.wakeUpSession(permission, complete: { (autState, error) in
                    if autState == .authorized {
                        self.quitButton.alpha = 1
                        self.actionButton.setTitle("Войти", for: UIControlState()) 
                    } else {
                        self.quitButton.alpha = 0
                        self.actionButton.setTitle("Авторизация", for: UIControlState())
                    }

                })
            }
        }
        catch {}
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        do {
            self.reach!.whenReachable = { reachability in
                DispatchQueue.global().async{
                    print("reachable")
                    OperationQueue.main.addOperation(){
                        UIView.animate(withDuration: self.animationTime, animations: {
                            self.internetConnectionEnabled()
                        })
                    }
                }
            }
            self.reach!.whenUnreachable = { reachability in
                DispatchQueue.global().async{
                    print("not reachable")
                    OperationQueue.main.addOperation(){
                        UIView.animate(withDuration: self.animationTime, animations: {
                            self.internetConnectionDisabled()
                        })
                    }
                }
            }
            try self.reach?.startNotifier()
        }
        catch  {
            print("error in Reachable")
        }

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.reach?.stopNotifier()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func internetConnectionDisabled() {
        self.actionButton.setTitle("Автономный режим", for: UIControlState())
        self.actionButtonWidth.constant += self.originButtonWidth / 2
        self.actionButton.frame.size = CGSize(width: self.actionButtonWidth.constant, height: self.actionButton.frame.height)
        self.actionButton.backgroundColor = UIColor.gray
        self.quitButton.isEnabled = false
        self.quitButton.alpha = VKSdk.accessToken() == nil ? 0 : 0.5
        self.informationLabel.alpha = 1
    }
    
    func internetConnectionEnabled() {
        if self.actionButton.titleLabel?.text == "Автономный режим"  {
            let newTitleText = VKSdk.accessToken() == nil ? "Авторизация" : "Войти"
            self.actionButton.setTitle(newTitleText, for: UIControlState())
            self.actionButtonWidth.constant -= self.originButtonWidth / 2
            self.actionButton.frame.size = CGSize(width: self.actionButtonWidth.constant, height: self.actionButton.frame.height)
            self.actionButton.backgroundColor = UIColor.green
            self.quitButton.isEnabled = VKSdk.accessToken() == nil ? false : true
            self.informationLabel.alpha = 0
        }
    }
    
    @IBAction func actionButtonTaped(_ sender: AnyObject) {
        if self.actionButton.titleLabel?.text == "Автономный режим" {
            self.performSegue(withIdentifier: "showUserAlbums", sender: nil)
            return
        }
        if  VKSdk.accessToken() == nil {
            VKSdk.authorize(self.permission, with: VKAuthorizationOptions.unlimitedToken)
        }
        else {
            self.performSegue(withIdentifier: "showUserAlbums", sender: nil)
        }
    }
    
   
    @IBAction func exitButtonTaped(_ sender: AnyObject) {
        VKSdk.forceLogout()
        UIView.animate(withDuration: self.animationTime, animations: {
            self.quitButton.alpha = 0
            self.actionButton.setTitle("Авторизация", for: UIControlState())
        }) 
       
    }
}

extension LaunchViewController: VKSdkDelegate {
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult) {
        if result.token != nil {
            self.navigationController?.popViewController(animated: true)
            self.performSegue(withIdentifier: "showUserAlbums", sender: nil)
        }
        else if result.error != nil {
            print("User canceled authorization, or occured unresolving networking error. Reset your UI to initial state and try authorize user later")
        }
        
    }
    
    func vkSdkUserAuthorizationFailed() {
    }
    
    func vkSdkTokenHasExpired(_ expiredToken: VKAccessToken!) {
    }
}

extension LaunchViewController: VKSdkUIDelegate {
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.present(controller, animated: true, completion: nil)
        print("1")
    }
    func vkSdkDidDismiss(_ controller: UIViewController!) {
        print("2")
    }
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        let vc = VKCaptchaViewController.captchaControllerWithError(captchaError)
        vc?.present(in: self)
    }
    
    func vkSdkWillDismiss(_ controller: UIViewController!) {
    }
}
