//
//  PhotoViewController.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit
import VK_ios_sdk
import PKHUD
import Reachability
import CWStatusBarNotification

class PhotoViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoTextView: UITextView!
    @IBOutlet weak var photoTextViewHeight: NSLayoutConstraint!
    
    
    internal var photo: Photo!
    fileprivate let imageTransitionSpeed = 0.4
    fileprivate let requestAttempts = 1
    fileprivate let downloadIndicator = UIActivityIndicatorView()
    fileprivate let permission = ["photos", "wall"]
    fileprivate let notification = CWStatusBarNotification()
    fileprivate var reachability: Reachability!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            self.reachability = try Reachability()
        }
        catch {
            print("Ошибка при инициализации reachabilityForInternetConnection")
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        self.downloadIndicator.hidesWhenStopped = true
        self.downloadIndicator.startAnimating()
        downloadIndicator.frame.origin = CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height / 2)
        self.view.insertSubview(self.downloadIndicator, at: 0)
        
        let url = URL(string: photo.imageUrl)
        self.photoImageView.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.global(), imageTransition: UIImageView.ImageTransition.crossDissolve(imageTransitionSpeed), runImageTransitionIfCached: false, completion: { (comp) in
            if comp.result.isSuccess {
                self.photo.imageData = UIImagePNGRepresentation(comp.result.value!)
            } else {
                if let savedData = self.photo.imageData {
                    self.photoImageView.image = UIImage(data: savedData)
                }
                else {
                    self.photoImageView.image = UIImage(named: "photoNotSaved")
                }
            }
            self.downloadIndicator.stopAnimating()
        })
        
        if photo.name == "" {
            photoTextView.alpha = 0
        }
        else {
            photoTextView.text = photo.name
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action , target: self, action: #selector(showActionSheet))
    }
    
    func checkReachability() -> Bool {
        if self.reachability?.isReachable == false {
            self.notification.display(withMessage: "Отсутствует подключение к интернету", forDuration: 1.0)
            return false
        } else {
            
            //VKSdk.wakeUpSession(permission, completeBlock: nil)
            return true
        }
        
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
        let shareAction = UIAlertAction(title: "На стену", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if !self.checkReachability() {
                return
            }
            HUD.show(.progress)
            DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
                let userId = VKSdk.accessToken().userId
                let photoAttachment = "photo\(userId)_\(self.photo.id.intValue)"
                let wallPostRequest =  VKApi.wall().post([VK_API_ATTACHMENTS : photoAttachment, VK_API_OWNER_ID : userId, "media_id" : 5679917])
                wallPostRequest?.attempts = Int32(self.requestAttempts)
                wallPostRequest?.execute(resultBlock: { (vkPostResponse) in
                        HUD.flash(.success, delay: 1.0)
                    }, errorBlock: { (requestError) in
                        print("error in wall.post request. errCode:\(requestError?.localizedDescription)")
                        HUD.flash(.error, delay: 1.0)
                })
            }
        })
        
        let openMapAction = UIAlertAction(title: "Показать на карте", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
                self.performSegue(withIdentifier: "showMap", sender: self)
        })
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        if self.photo.geoLong != nil {
            actionSheet.addAction(openMapAction)
        }
        actionSheet.addAction(shareAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let destVC = segue.destination as! MapViewController
            destVC.geoLat = self.photo.geoLat!.doubleValue
            destVC.geoLong = self.photo.geoLong!.doubleValue
        }
    }
}
