//
//  UserAlbumsTableViewController.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit
import VK_ios_sdk
import Reachability
import CWStatusBarNotification

class UserAlbumsTableViewController: UITableViewController {
    
    fileprivate var reachability: Reachability!
    fileprivate var albums: [Album]!
    fileprivate let numberOfRowInTable = 6
    fileprivate let requestAttempts = 1
    fileprivate let downloadIndicator = UIActivityIndicatorView()
    fileprivate let notification = CWStatusBarNotification()
    fileprivate let permission = ["photos", "wall"]
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            self.reachability = try Reachability()
        }
        catch {
            print("Ошибка при инициализации reachabilityForInternetConnection")
        }

        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = "Альбомы"
        self.tableView.rowHeight = tableView.frame.height / CGFloat(numberOfRowInTable)
        let footerView = UIView()
        self.tableView.tableFooterView = footerView
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.attributedTitle = NSAttributedString(string: "Обновить")
        self.refreshControl!.addTarget(self, action: #selector(refreshAlbums), for: UIControlEvents.valueChanged)
        self.tableView.setContentOffset(CGPoint(x: 0, y: -1 * refreshControl!.frame.size.height), animated: true)
        
        if let savedAlbums = CoreDataManager.loadObjectsForEntity(entityName: "Album") as? [Album] {
            self.albums = savedAlbums
        } else if  true /*self.reachability?.isReachable() == true*/ {
            self.albums = []
            self.downloadIndicator.hidesWhenStopped = true
            self.downloadIndicator.startAnimating()
            downloadIndicator.frame.origin = CGPoint(x: self.tableView.frame.width / 2, y: self.tableView.frame.height / 3)
            self.tableView.insertSubview(self.downloadIndicator, at: 0)
            downloadAlbums()
        }
    }
    
    func checkReachability() -> Bool {
        if self.reachability?.isReachable == false {
            self.refreshControl?.endRefreshing()
            self.notification.display(withMessage: "Отсутствует подключение к интернету", forDuration: 1.0)
            return false
        } else {
            VKSdk.wakeUpSession(permission, complete: nil)
            self.refreshControl?.endRefreshing()
            return true
        }
        
    }
    
    func updateAlbumsArray(_ masterArray : [NSDictionary]) {
        refreshControl?.attributedTitle = NSAttributedString(string: "Обновляю")
        let masterAlbumIds = NSMutableOrderedSet()
        masterArray.forEach { (album) in
            masterAlbumIds.add(album["id"] as! Int)
        }
        tableView.beginUpdates()
        var index = 0
        let currentSavedAlbumsIds = NSMutableOrderedSet()
        var rowsPathForDelete = [IndexPath]()
        while index != self.albums.count {
            if !masterAlbumIds.contains(self.albums[index].id.intValue) {
                CoreDataManager.deleteObject(self.albums[index])
                self.albums.remove(at: index)
                rowsPathForDelete.append(IndexPath(row: index + rowsPathForDelete.count, section: 0))
            } else {
                currentSavedAlbumsIds.add(self.albums[index].id.intValue)
                index += 1
            }
        }
        self.tableView.deleteRows(at: rowsPathForDelete, with: .fade)
        
        for i in 0..<masterArray.count {
            let masterAlbumId = (masterArray[i]["id"] as! NSNumber).intValue
            if i >= self.albums.count || !currentSavedAlbumsIds.contains(masterAlbumId) {
                let newAlbum = CoreDataManager.createObjectForEntity(entityName: "Album") as! Album
                newAlbum.configureBy(dictionary: masterArray[i])
                self.albums.append(newAlbum)
                self.tableView.insertRows(at: [IndexPath(row: self.albums!.count - 1, section: 0)], with: .fade)
            }
        }
        tableView.endUpdates()
        CoreDataManager.save()
        refreshControl?.endRefreshing()
    }
    
    func refreshAlbums() {
        if !checkReachability() {
            return
        }

        
        if self.albums.count == 0 {
            downloadAlbums()
        } else {
            refreshControl?.attributedTitle = NSAttributedString(string: "Загружаю")
            DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
                print(VKSdk.accessToken().userId)
                let albumsRequest = VKApi.request(withMethod: "photos.getAlbums", andParameters: [VK_API_OWNER_ID: VKSdk.accessToken().userId, "need_covers" : 1, "photo_sizes" : 1])
                albumsRequest?.attempts = Int32(self.requestAttempts)
                albumsRequest?.execute(resultBlock: { (requestResponce) in
                    let jsonResult = requestResponce?.json as? NSDictionary
                    self.downloadIndicator.stopAnimating()
                    if let jsonResultItems = jsonResult?["items"] as? [NSDictionary] {
                        self.updateAlbumsArray(jsonResultItems)
                    }
                }, errorBlock: { (requestError) in
                   // print("error in photos.getAlbums request. errCode:\(requestError.code)")
                    self.refreshControl?.endRefreshing()
                    self.downloadIndicator.stopAnimating()
                    self.notification.display(withMessage: "Что-то пошло не так при загрузке", forDuration: 1.0)
                })
            }
        }
    }
    
    func downloadAlbums() {
        if !checkReachability() {
            return
        }

        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            let albumsRequest = VKApi.request(withMethod: "photos.getAlbums", andParameters: [VK_API_OWNER_ID: VKSdk.accessToken().userId, "need_covers" : 1, "photo_sizes" : 1])
            albumsRequest?.attempts = Int32(self.requestAttempts)
            albumsRequest?.execute(resultBlock: {(vkresp) in
                let jsonResult =  vkresp?.json as! NSDictionary
                print("Получен ответ count:\(jsonResult["count"] as! Int)")
                self.downloadIndicator.stopAnimating()
                if let jsonResultItems = jsonResult["items"] as? [NSDictionary] {
                    self.tableView.beginUpdates()
                    for i in 0..<jsonResultItems.count {
                        let newAlbum = CoreDataManager.createObjectForEntity(entityName: "Album") as! Album
                        newAlbum.configureBy(dictionary: jsonResultItems[i])
                        self.albums.append(newAlbum)
                        self.tableView.insertRows(at: [IndexPath(row: self.albums!.count - 1, section: 0)], with: .fade)
                    }
                    self.tableView.endUpdates()
                    CoreDataManager.save()
                    self.refreshControl?.endRefreshing()
                }
            }) { (requestError) in
               // print("error in photos.getAlbums request. errCode:\(requestError.code)")
                self.refreshControl?.endRefreshing()
                self.downloadIndicator.stopAnimating()
                self.notification.display(withMessage: "Что-то пошло не так при загрузке", forDuration: 1.0)
            }
        }
    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAlbumPhotos" {
            let destVC = segue.destination as! AlbumPhotosTableViewController
            destVC.album = self.albums![tableView.indexPathForSelectedRow!.row]
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let rowCount = albums?.count {
            return rowCount
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumTableViewCell
        cell.configureBy(album: self.albums![indexPath.row], height: tableView.rowHeight * 0.8)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showAlbumPhotos", sender: self)
    }
}
