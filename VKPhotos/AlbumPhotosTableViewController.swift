//
//  AlbumPhotosTableViewController.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit
import VK_ios_sdk
import CWStatusBarNotification
import CoreData
import Reachability
//import ReachabilitySwift

class AlbumPhotosTableViewController: UITableViewController {

    
    internal var album: Album!
    fileprivate let numberOfRowsInTable = 8
    fileprivate let requestAttempts = 1
    fileprivate let downloadIndicator = UIActivityIndicatorView()
    fileprivate let notification = CWStatusBarNotification()
    fileprivate var reachability: Reachability!
    fileprivate let permission = ["photos", "wall"]
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        do {
//            self.reachability = try Reachability.reachabilityForInternetConnection()
//        }
//        catch {
//            print("Ошибка при инициализации reachabilityForInternetConnection")
//        }
       
        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = album.name
        self.tableView.rowHeight = tableView.frame.height / CGFloat(self.numberOfRowsInTable)
        let footerView = UIView()
        tableView.tableFooterView = footerView
        refreshControl = UIRefreshControl()
        refreshControl!.attributedTitle = NSAttributedString(string: "Обновить")
        refreshControl!.addTarget(self, action: #selector(refreshPhotos), for: UIControlEvents.valueChanged)
         self.tableView.setContentOffset(CGPoint(x: 0, y: -1 * refreshControl!.frame.size.height), animated: true)
        
        if album.photos.count == 0 {
            self.downloadIndicator.hidesWhenStopped = true
            self.downloadIndicator.startAnimating()
            downloadIndicator.frame.origin = CGPoint(x: self.tableView.frame.width / 2, y: self.tableView.frame.height / 3)
            self.tableView.insertSubview(self.downloadIndicator, at: 0)
            downloadPhotos()
        }
    }
    
    func checkReachability() -> Bool {
        if self.reachability?.isReachable == false {
            self.refreshControl?.endRefreshing()
            self.notification.display(withMessage: "Отсутствует подключение к интернету", forDuration: 1.0)
            return false
        } else {
            
            //VKSdk.wakeUpSession(permission, completeBlock: nil)
            return true
        }
        return true
    }
    
    
    func downloadPhotos() {
        if !checkReachability() {
            return
        }
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            let albumPhotosRequest = VKApi.request(withMethod: "photos.get", andParameters: [VK_API_OWNER_ID: VKSdk.accessToken().userId, "album_id" : self.album.id.intValue, "photo_sizes" : 1])
            albumPhotosRequest?.attempts = Int32(self.requestAttempts)
            albumPhotosRequest?.execute(resultBlock: {(requestResponce) in
                let jsonResult =  requestResponce?.json as? NSDictionary
                self.downloadIndicator.stopAnimating()
                if let jsonResultItems = jsonResult?["items"] as? [NSDictionary] {
                    self.tableView.beginUpdates()
                    for i in 0..<jsonResultItems.count {
                        let newPhoto = CoreDataManager.createObjectForEntity(entityName: "Photo") as! Photo
                        newPhoto.configureBy(dictionary: jsonResultItems[i], album: self.album)
                        self.album.photos.add(newPhoto)
                        self.tableView.insertRows(at: [IndexPath(row: self.album.photos.count - 1, section: 0)], with: .fade)
                    }
                    self.tableView.endUpdates()
                    CoreDataManager.save()
                }
            }) { (requestError) in
               // print("error in photos.get request. errCode:\(requestError.code)")
                self.refreshControl?.endRefreshing()
                self.downloadIndicator.stopAnimating()
                self.notification.display(withMessage: "Что-то пошло не так при загрузке", forDuration: 1.0)
            }
        }
    }
    
    
    func updatePhotosArray(_ masterArray : [NSDictionary]) {
        refreshControl?.attributedTitle = NSAttributedString(string: "Обновляю")
        let masterPhotoIds = NSMutableOrderedSet()
        masterArray.forEach { (photo) in
            masterPhotoIds.add(photo["id"] as! Int)
        }
        tableView.beginUpdates()
        var index = 0
        let currentSavedPhotoIds = NSMutableOrderedSet()
        var rowsPathForDelete = [IndexPath]()
        print(self.album.photos.count)
        while index != self.album.photos.count {
            if !masterPhotoIds.contains((self.album.photos[index] as AnyObject).id.intValue) {
                CoreDataManager.deleteObject(self.album.photos.object(at: index) as! NSManagedObject)
                rowsPathForDelete.append(IndexPath(row: index + rowsPathForDelete.count, section: 0))
            } else {
                currentSavedPhotoIds.add((self.album.photos.object(at: index) as! Photo).id.intValue)
                index += 1
            }
        }
        self.tableView.deleteRows(at: rowsPathForDelete, with: .fade)
        
        for i in 0..<masterArray.count {
            let masterPhotoId = (masterArray[i]["id"] as! NSNumber).intValue
            if i >= self.album.photos.count || !currentSavedPhotoIds.contains(masterPhotoId) {
                let newPhoto = CoreDataManager.createObjectForEntity(entityName: "Photo") as! Photo
                newPhoto.configureBy(dictionary: masterArray[i], album: self.album)
                self.album.photos.add(newPhoto)
                self.tableView.insertRows(at: [IndexPath(row: self.album.photos.count - 1, section: 0)], with: .fade)
            }
        }
        CoreDataManager.save()
        tableView.endUpdates()
        refreshControl?.endRefreshing()
    }
    
    func refreshPhotos() {
        if !checkReachability() {
            return
        }

        
        if self.album.photos.count == 0 {
            downloadPhotos()
        } else {
            refreshControl?.attributedTitle = NSAttributedString(string: "Загружаю")
            let albumPhotosRequest = VKApi.request(withMethod: "photos.get", andParameters: [VK_API_OWNER_ID: VKSdk.accessToken().userId,  "album_id" : self.album.id.intValue, "photo_sizes" : 1])
            albumPhotosRequest?.attempts = Int32(self.requestAttempts)
            albumPhotosRequest?.execute(resultBlock: {(requestResponce) in
                    let jsonResult = requestResponce?.json as! NSDictionary
                    self.downloadIndicator.stopAnimating()
                    if let jsonResultItems = jsonResult["items"] as? [NSDictionary] {
                        self.updatePhotosArray(jsonResultItems)
                    }
                    }, errorBlock: { (requestError) in
                      //  print("error in photos.getAlbums request. errCode:\(requestError.code)")
                        self.refreshControl?.endRefreshing()
                        self.downloadIndicator.stopAnimating()
                        self.notification.display(withMessage: "Что-то пошло не так при загрузке", forDuration: 1.0)
                })
            }
    }
    
    
    // MARK: - Table view data source
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto" {
            let destVC = segue.destination as! PhotoViewController
            destVC.photo = self.album.photos[tableView.indexPathForSelectedRow!.row] as! Photo
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return album.photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! AlbumPhotoTableViewCell
        cell.configureBy(photo: self.album.photos[indexPath.row] as! Photo, height: tableView.rowHeight * 0.8)
        cell.photoNameTextView.isUserInteractionEnabled = false
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showPhoto", sender: self)
    }


}
