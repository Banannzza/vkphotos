//
//  MapViewController.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 24/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {

    internal var geoLat: Double!
    internal var geoLong: Double!
    
    override func viewWillAppear(_ animated: Bool) {
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera( withLatitude: geoLat, longitude: geoLong, zoom: 10)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        view = mapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: geoLat, longitude: geoLong)
        marker.map = mapView
    }
}
