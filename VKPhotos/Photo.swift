//
//  Photo.swift
//  VKPhotos
//
//  Created by Алексей Остапенко on 23/10/16.
//  Copyright © 2016 Алексей Остапенко. All rights reserved.
//

import Foundation
import CoreData

class Photo: NSManagedObject {
    @NSManaged var id: NSNumber
    @NSManaged var name: String
    @NSManaged var previewImageUrl: String
    @NSManaged var previewImageData: Data?
    @NSManaged var imageUrl: String
    @NSManaged var imageData: Data?
    @NSManaged var date: Date
    @NSManaged var album: Album
    @NSManaged var geoLat: NSNumber?
    @NSManaged var geoLong: NSNumber?
    
    func configureBy(dictionary: NSDictionary, album: Album){
        self.id = dictionary["id"] as! NSNumber
        self.album = album
        self.name = dictionary["text"] as! String
        let intDate = dictionary["date"] as! Int
        
        if let geoPositionLat = dictionary["lat"] as? Double {
            self.geoLat = geoPositionLat as NSNumber
            self.geoLong = dictionary["long"] as! Double as NSNumber
        } else {
            self.geoLat = nil
            self.geoLong = nil
        }
        
        self.date = Date.init(timeIntervalSince1970: TimeInterval(intDate))
        var maxImageWidth = 0
        for image in dictionary["sizes"] as! [NSDictionary] {
            let currentWidth =  image["width"] as! Int
            let imageType = image["type"] as! String
            if imageType == "x" {
                self.previewImageUrl = image["src"] as! String
            }
            if currentWidth >= maxImageWidth {
                self.imageUrl = image["src"] as! String
                maxImageWidth = currentWidth
            }
        }
    }
}
